import React from "react";

import NavigationMenu from '../navigation-menu/NavigationMenu';
import BreadcrumbsNavigation from "../breadcrumbs-navigation/BreadcrumbsNavigation";
import {Menu} from "../menu/Menu";
import { FilesContainer } from "../files-container/FilesContainer";
import jsonData from '../data/simulation-data.json';

import './Page.css'


class Page extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fileName: this.props.location.pathname,
            newDataFromChild: jsonData
        }
    }

    componentDidMount() {
        this.props.history.listen((location) => {
            this.setState({ fileName: location.pathname })
        });
    }

    handleNewDataFromChild = (dataFromChild) => {
        this.setState({newDataFromChild: dataFromChild})
    }

    searchChildrenData(data, index = 0) {
        let fullPathArray = this.state.fileName.split('/').filter(e => e)
        let searchedFile
        data.forEach(el => {
            if (el.name === fullPathArray[index]) {
                searchedFile = el
            }
        })

        if (searchedFile) {
            if (index < fullPathArray.length - 1) {
                if (searchedFile.type === "folder") {
                    return this.searchChildrenData(searchedFile.children, index + 1)
                }
            } else {
                return searchedFile.type
                 
            }
        } 
    }

    render() {
        let typeOfCurrentFile = this.searchChildrenData(this.state.newDataFromChild)

        return <div className='page-div'>
            <div className='navigation-menu-header'>
                <NavigationMenu />
                <div className="header">
                    <BreadcrumbsNavigation fileName={this.state.fileName} />
                    {
                        (typeOfCurrentFile === "folder") && 
                            <Menu parentPageCallback={this.handleNewDataFromChild} currentPath={this.state.fileName} />
                    }
                </div>
            </div>
            <div className="files-div" >
                <FilesContainer newDataFromChild={this.state.newDataFromChild} fileName={this.state.fileName} />
            </div>
        </div>;
    }
}

export { Page }