import React from 'react'
import { Redirect } from 'react-router';

import json from '../data/simulation-data.json'
import FileRow from '../file-row/FileRow';
import { FileContent } from '../file-content/FileContent';

const NO_DATA_FOUND = -1

class FilesContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            jsonData: this.props.newDataFromChild ? this.props.newDataFromChild : json
        };
    }

    searchChildrenData(data, index = 0) {
        let fullPathArray = this.props.fileName.split('/').filter(e => e)
        let searchedFile
        data.forEach(el => {
            if (el.name === fullPathArray[index]) {
                searchedFile = el
            }
        })

        if (searchedFile) {
            if (index < fullPathArray.length - 1) {
                if (searchedFile.type === "folder") {
                    return this.searchChildrenData(searchedFile.children, index + 1)
                } 
            } else if (searchedFile.type === "file") {
                return searchedFile.fileContent
            } else {
                let childrenData = searchedFile.children
                return childrenData
            }
        } else {
            return NO_DATA_FOUND
        }
    }

    handleNewDataCallback = (dataFromChild) =>{
        this.setState({newChildrenData: dataFromChild})
    }

    renderRows(fileList) {
        return fileList.map((data) => {
            return (
                    <FileRow
                        key={data.id}
                        id={data.id}
                        type={data.type}
                        name={data.name}
                        commit={data.commit}
                        time={data.time}
                        fileName={this.props.fileName}
                        paddingLeft={this.props.paddingLeft}
                        parentFilesContainerCallback = {this.handleNewDataCallback}
                    />
            )
        })
    }

    render() {
        let arrayFolderType = []
        let arrayFileType = []
        let searchedData = this.searchChildrenData(this.state.jsonData)

        if (searchedData === NO_DATA_FOUND) {
            return (<Redirect to="/error" />)
        } else if (typeof(searchedData) !== 'object') {
            return <FileContent fileContent={searchedData} />
        } else {
            searchedData.forEach((el) => {
                (el.type === "folder") ? arrayFolderType.push(el) : arrayFileType.push(el)
            })
        }

        return (
            <div>
                {
                    this.renderRows(arrayFolderType.sort((first, second) => {
                        return (first.name < second.name) ? -1 : 1
                    }))
                }
                {
                    this.renderRows(arrayFileType.sort((first, second) => {
                        return (first.name < second.name) ? -1 : 1
                    }))
                }
            </ div>
        )
    }
}

export { FilesContainer };