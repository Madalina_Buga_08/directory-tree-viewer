import React from "react";

import { OPEN_BRACKET_FOUND, CLOSE_BRACKET_FOUND, NO_BRACKET_FOUND, ADD_CONTRACT_ICON } from '../../constants/file-content.js';

import { CodeLine } from "../code-line/CodeLine";
import "./FileContent.css";


class FileContent extends React.Component {

    searchRowsBlocks = (parenthesesFound, extendedRowsNumber) => {
        let openFoundIndex = NO_BRACKET_FOUND
        parenthesesFound.forEach((el, index) =>
        {
            if (el === OPEN_BRACKET_FOUND) {
                openFoundIndex = index
            } else if (el === CLOSE_BRACKET_FOUND) {
                parenthesesFound[index] = NO_BRACKET_FOUND
                extendedRowsNumber[openFoundIndex] = index - openFoundIndex
                parenthesesFound[openFoundIndex] = ADD_CONTRACT_ICON
                return this.searchRowsBlocks(parenthesesFound, extendedRowsNumber)
                } 
            })
        return extendedRowsNumber
    }

    render() {
        let fileContent = this.props.fileContent.split('\n')
        let parenthesesFound = []
        fileContent.forEach((el, index) => {
            if (el.search("{") !== -1 && el.search("}") !== -1) {
                parenthesesFound[index] = NO_BRACKET_FOUND
            } else if (el.search("{") !== -1) {
                parenthesesFound[index] = OPEN_BRACKET_FOUND
            } else if (el.search("}") !== -1) {
                parenthesesFound[index] = CLOSE_BRACKET_FOUND
            } else {
                parenthesesFound[index] = NO_BRACKET_FOUND
            }
        })
        let extendedRowsNumber = new Array(fileContent.length).fill(0)
        extendedRowsNumber = this.searchRowsBlocks(parenthesesFound, extendedRowsNumber)

        return (
            <div className="file-content-box">
                {
                    fileContent.map((el, index) => 
                        <CodeLine 
                            key={index}
                            element = {el} 
                            indexElement = {index}
                            parenthesesFound = {parenthesesFound}
                            extendedRowsNumber = {extendedRowsNumber}
                            />
                    )
                }
            </div>
        )
    }
}

export { FileContent }