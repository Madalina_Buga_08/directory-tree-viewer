import React from "react";
import { withRouter } from "react-router";

import LeftArrowIcon from '../../assets/svg/left-arrow.svg';
import RightArrowIcon from '../../assets/svg/right-arrow.svg';
import UpArrowIcon from '../../assets/svg/up-arrow.svg';
import "./NavigationMenu.css";


class NavigationMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
           backNav: [this.props.location.pathname],
           forwardNav: []
        }
    }

    componentDidMount() {
        this.props.history.listen((location) => {
            let lastLocation = this.state.backNav[this.state.backNav.length-1]
            if (lastLocation !== location.pathname) {
                this.setState({ backNav: [...this.state.backNav, location.pathname] })
            }
        });
    }

    handleOnClickToParentFolder = () => {
        let currentPath = this.state.backNav[this.state.backNav.length-1].split('/').filter(e=>e)
        // check if there is a parent folder
        if (currentPath.length > 1) {
            currentPath.pop()
            let parentFolder = '/' + currentPath.join('/')
            this.props.history.replace(parentFolder)
        } 
    }

    handleOnClickForward = () => {
        let forwardPath = [...this.state.forwardNav]
        this.props.history.replace(forwardPath[forwardPath.length-1])
        // check if there is more than one path 
        if (forwardPath.length > 1) {
            forwardPath.pop()
            this.setState({ forwardNav: [...forwardPath] })
        } 
    }

    handleOnClickBack = () => {
        let backPath = [...this.state.backNav]
        // check if there is more than one path
        if (backPath.length > 1) {
            this.setState({ forwardNav: [...this.state.forwardNav, this.state.backNav[this.state.backNav.length-1]] })
            backPath.pop()
            this.props.history.replace(backPath[backPath.length-1])
            this.setState({ backNav: [...backPath] });
        } 
    }

    render() {
        return (
            <div className="navigation-menu">
                <img onClick={this.handleOnClickBack} className="navigation-menu-icon" src={LeftArrowIcon} alt="left arrow icon" />
                <img onClick={this.handleOnClickForward} className="navigation-menu-icon" src={RightArrowIcon} alt="right arrow icon" />
                <img onClick={this.handleOnClickToParentFolder} className="navigation-menu-icon" src={UpArrowIcon} alt="up arrow icon" />
            </div>
        )
    }
}

export default withRouter(NavigationMenu)