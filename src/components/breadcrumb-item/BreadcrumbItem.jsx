import React from 'react';
import { withRouter } from 'react-router-dom';

import './BreadcrumbItem.css';


class BreadcrumbItem extends React.Component {
    render() {
        return (
            <div className="clickable-path-element" onClick={() => this.props.history.replace(this.props.URLPath)}>
                <span className="link-element">{this.props.itemName}</span> /
            </div>
        );
    }
}

export default withRouter(BreadcrumbItem)