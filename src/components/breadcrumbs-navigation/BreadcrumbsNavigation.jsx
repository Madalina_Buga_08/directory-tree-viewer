import React from 'react';
import { withRouter } from 'react-router-dom';

import BreadcrumbItem from '../breadcrumb-item/BreadcrumbItem';
import './BreadcrumbsNavigation.css';


class BreadcrumbsNavigation extends React.Component {

    render() {

        let path = [...this.props.fileName.split('/').filter(e => e)]
        let pathLength = path.length
        let newPath = path.slice(0, pathLength - 1)

        return <div className="breadcrumbs-container">
            {
                newPath.map((link) => {
                    return <BreadcrumbItem
                        key={link}
                        URLPath={"/" + path.join('/').slice(0, path.join('/').indexOf(link)) + link}
                        itemName={link}
                    />
                })
            }
            <div className="path-element" >{path[pathLength - 1]} / </div>
        </div >;
    }
}

export default withRouter(BreadcrumbsNavigation);