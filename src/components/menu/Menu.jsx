import React from "react";

import { NewFileModal } from "../new-file-modal/NewFileModal";

import AddFolderIcon from '../../assets/svg/add-folder.svg';
import AddFileIcon from '../../assets/svg/add-file.svg';
import "./Menu.css";


class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            dataFromChild: null,
            fileType: ""
        }
    }

    handleCallback = (childConfirm) => {
        this.setState({isModalOpen: childConfirm})
    }
    handleNewDataCallback = (childData) => {
        this.setState({dataFromChild: childData})
        this.handleCallbackToPage(childData);
    }
    handleCallbackToPage = (data) => {
        this.props.parentPageCallback(data);
    }

    renderModalForm(){
        if (this.state.isModalOpen === true) {
            return <NewFileModal 
                        currentPath={this.props.currentPath} 
                        parentForNewFileDataCallback={this.handleNewDataCallback} 
                        parentCallback={this.handleCallback} 
                        fileType={this.state.fileType} 
                    />
        }
    }

    render() {
        return <div className="menu-icons-div">

            <div onClick={() => this.setState({isModalOpen: true, fileType: "folder"})} className="menu-element">
                <img className="menu-icon" src={AddFolderIcon} alt="add new folder icon" />
            </div>

            <div onClick={() => this.setState({isModalOpen: true, fileType: "file"})} className="menu-element">
                <img className="menu-icon" src={AddFileIcon} alt="add new file icon" />
            </div>
            {
                this.renderModalForm()
            }

        </div>
    }
}

export { Menu }