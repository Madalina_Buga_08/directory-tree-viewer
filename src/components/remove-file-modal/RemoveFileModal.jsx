import React from "react";

import jsonData from '../data/simulation-data.json'

import "./RemoveFileModal.css";


class RemoveFileModal extends React.Component {

    searchChildrenData(data, removeSelectedItem, index = 0) {
        let fullPathArray = this.props.pathItem.split('/').filter(e => e)
        let searchedFile
        data.forEach(el => {
            if (el.name === fullPathArray[index]) {
                searchedFile = el
            }
        })

        if (searchedFile) {
            if (index < fullPathArray.length - 1) {
                if (searchedFile.type === "folder") {
                    return this.searchChildrenData(searchedFile.children, removeSelectedItem, index + 1)
                }
            } else {
                if (removeSelectedItem === false) {
                    let childrenData = searchedFile.children
                    return childrenData
                } else {
                    this.onRemoveTrigger()
                    let childrenData = searchedFile.children
                    let indexItemChild = null
                    childrenData.forEach((el, index) => {
                        if(el.id === this.props.idItem) {
                            indexItemChild = index;
                        }
                    })
                    childrenData.splice(indexItemChild, 1)
                    this.onRemoveDataTrigger(jsonData)
                }
            }
        } 
    }

    onRemoveTrigger = () => {
        this.props.parentForRemoveCallback(false);
    }
    onRemoveDataTrigger = (data) => {
        this.props.parentForRemoveDataCallback(data);
    }


    render() {
        let removeSelectedItem = false
        let searchedData = this.searchChildrenData(jsonData, removeSelectedItem)
        let itemType = "";
        let itemName = "";
        searchedData.forEach((el) => {
            if(el.id === this.props.idItem) {
                itemType = el.type;
                itemName = el.name;
            }
        })
            return (
                <div className="remove-file-modal">
                    <div className="remove-modal-content">

                        <div>
                            Are you sure you want to remove 
                            <span className="file-name-span">{" " + itemName + " "}</span> 
                            {itemType}?
                        </div>

                        <div className="remove-modal-buttons-container">
                            <div onClick = {() => this.searchChildrenData(jsonData, removeSelectedItem = true)} className="confirm-remove-btn">YES</div>
                            <div onClick = {this.onRemoveTrigger} className="remove-cancel-btn">CANCEL</div>
                        </div>
                    </div>
                </div>
        )
    }
}

export { RemoveFileModal }