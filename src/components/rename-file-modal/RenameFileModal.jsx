import React from "react";

import jsonData from '../data/simulation-data.json';

import "./RenameFileModal.css";


class RenameFileModal extends React.Component {

    onRenameTrigger = () => {
        this.props.parentForRenameCallback(false);
    }
    onRenameDataTrigger = (data) => {
        this.props.parentForRenameDataCallback(data);
    }

    changeFolderName(data, newName, index = 0) {
        let fullPathArray = this.props.pathItem.split('/').filter(e => e)
        let searchedFile
        data.forEach(el => {
            if (el.name === fullPathArray[index]) {
                searchedFile = el
            }
        })

        if (searchedFile) {
            if (index < fullPathArray.length - 1) {
                if (searchedFile.type === "folder") {
                    return this.changeFolderName(searchedFile.children, newName, index + 1)
                }
            } else {
                this.onRenameTrigger()
                let childrenData = searchedFile.children
                childrenData.forEach((el) => {
                    if(el.id === this.props.idItem) {
                        el.name = newName
                    }
                })
                this.onRenameDataTrigger(jsonData)
            }
        } 
    }

    render() {
        let newName = ""
            return (
                <div className="rename-file-modal">
                    <div className="rename-modal-content">

                        <input onChange={(e) => {newName=e.target.value}} type="text" placeholder="Enter new name" />

                        <div className="rename-modal-buttons-container">

                            <div 
                                onClick={() => this.changeFolderName(jsonData, newName)}
                                className="confirm-rename-btn"
                            >APPLY</div>

                            <div 
                                onClick = {this.onRenameTrigger}
                                className="rename-cancel-btn"
                            >CANCEL</div>

                        </div>

                    </div>
                </div>
        )
        
    }
}

export { RenameFileModal }