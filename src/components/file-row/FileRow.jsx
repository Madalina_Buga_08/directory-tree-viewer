import React from 'react'
import { Link, withRouter } from 'react-router-dom';
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";

import { FilesContainer } from '../files-container/FilesContainer';
import { RemoveFileModal } from '../remove-file-modal/RemoveFileModal';
import { RenameFileModal } from '../rename-file-modal/RenameFileModal';
import { displayTimeAgo } from '../../utils/time-utils.js';

import FolderIcon from '../../assets/svg/folder.svg';
import FileIcon from '../../assets/svg/files.svg';
import PlusIcon from '../../assets/svg/plus.svg';
import MinusIcon from '../../assets/svg/minus.svg';
import EditIcon from '../../assets/svg/edit.svg';
import DeleteIcon from '../../assets/svg/delete.svg';
import './FileRow.css';


class FileRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isRowExpanded: false,
            isRemoveFileModalOpen: false,
            isRenameFileModalOpen: false,
            childrenData: null,
            pathItem: "",
            idItem: null
        };
    }

    addIconForFile(type) {
        if (type === "folder") {
            return <div style={{ paddingLeft: this.props.paddingLeft + 'rem' }}>
                <img
                    onClick={this.changeExpandIconForFolder}
                    className="expand-icon"
                    src={this.state.isRowExpanded ? MinusIcon : PlusIcon}
                    alt="plus icon" />
                <img className="folder-icon" src={FolderIcon} alt="Folder Icon" />
            </div>
        } else {
            return <img style={{ paddingLeft: this.props.paddingLeft + 'rem' }} className="file-icon" src={FileIcon} alt="File Icon" />
        }
    }

    changeExpandIconForFolder = () => {
        this.setState((this.state.isRowExpanded === false) ? { isRowExpanded: true } : { isRowExpanded: false });
    }

    expandFolderContent() {
        if (this.state.isRowExpanded === true) {
            return (
                <FilesContainer
                    paddingLeft={this.props.paddingLeft ? this.props.paddingLeft + 2 : 2}
                    fileName={this.props.fileName + '/' + this.props.name}
                />
            )
        }
    }

    handleRemoveCallback = (childConfirm) =>{
        this.setState({isRemoveFileModalOpen: childConfirm})
    }
    handleRenameCallback = (childConfirm) =>{
        this.setState({isRenameFileModalOpen: childConfirm})
    }
    handleRenameDataCallback = (dataFromChild) =>{
        this.setState({childrenData: dataFromChild});
        this.sendNewDataToFilesContainer(this.state.childrenData);
    }
    handleRemoveDataCallback = (dataFromChild) =>{
        this.setState({childrenData: dataFromChild});
        this.sendNewDataToFilesContainer(this.state.childrenData);
    }
    sendNewDataToFilesContainer = (data) => {
        this.props.parentFilesContainerCallback(data);
    }

    renderModal() {
        if(this.state.isRemoveFileModalOpen === true) {
            return <RemoveFileModal 
                        parentForRemoveCallback = {this.handleRemoveCallback} 
                        parentForRemoveDataCallback = {this.handleRemoveDataCallback}
                        idItem={this.state.idItem} 
                        pathItem={this.state.pathItem} 
                    />
        } else if(this.state.isRenameFileModalOpen === true) {
            return <RenameFileModal 
                        parentForRenameCallback = {this.handleRenameCallback} 
                        parentForRenameDataCallback = {this.handleRenameDataCallback}
                        idItem={this.state.idItem} 
                        pathItem={this.state.pathItem}
                    />
        }
      }

    render() {
        let urlLink = this.props.fileName;
        (urlLink !== '/') && (urlLink += '/');
        urlLink += this.props.name;

        return (
            <div>
                    <ContextMenuTrigger id={this.props.fileName + this.props.id}>
                        <div className="file-row">
                            <div className="type-name-element">
                                {
                                    this.addIconForFile(this.props.type)
                                }
                                <div>
                                    <Link
                                        to={urlLink}
                                        className="class-link"
                                    >{this.props.name}</Link>
                                </div>
                            </div>
                            <div className="commit-message-element">
                                <div className="commit-text">{this.props.commit}</div>
                            </div>
                            <div className="time-element">{displayTimeAgo(this.props.time)}</div>
                        </div>
                    </ContextMenuTrigger>
    
                    <ContextMenu className="context-menu" id={this.props.fileName + this.props.id}>
                        <div className="menu-item">
                            <MenuItem 
                                onClick={() => this.setState({isRenameFileModalOpen: true, pathItem: this.props.fileName, idItem: this.props.id})}>
                                Rename
                            </MenuItem>
                            <img className="context-menu-icon" src={EditIcon} alt="Edit Icon" />
                        </div>
                        <div className="menu-item">
                            <MenuItem 
                                onClick={() => this.setState({isRemoveFileModalOpen: true, pathItem: this.props.fileName, idItem: this.props.id})}>
                                Remove
                            </MenuItem>
                            <img className="context-menu-icon" src={DeleteIcon} alt="Delete Icon" />
                        </div>
                    </ContextMenu>

                {
                    this.expandFolderContent()
                }
                {
                    this.renderModal()
                }
            </div>
        )
    }
}

export default withRouter(FileRow);