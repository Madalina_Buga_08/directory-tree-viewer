import React from "react";
// import { Markup } from "interweave";

import { checkKeywords } from '../../utils/check-keywords.js';
import { ADD_CONTRACT_ICON } from '../../constants/file-content.js';

import MinusSquare from '../../assets/images/minus-square.png';
import PlusSquare from '../../assets/images/plus-square.png';
import "../file-content/FileContent.css";


class CodeLine extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isExpanded: true
        }
    }

    handleOnClickExpandCode = (indexLine, rowsNumber) => {
        if (this.state.isExpanded === true) {
            document.getElementById("img-"+indexLine).src = PlusSquare
            document.getElementById("remove-span-"+indexLine).classList.remove("remove-span")
            for (let i = 1; i <= rowsNumber; i++) {
                document.getElementById('code-line-'+(indexLine+i)).className = "remove-line"
            }
            this.setState({isExpanded: false})
        } else {
            document.getElementById("img-"+indexLine).src = MinusSquare
            document.getElementById("remove-span-"+indexLine).className = "remove-span"
            for (let i = 1; i <= rowsNumber; i++) {
                document.getElementById('code-line-'+(indexLine+i)).className = "code-line"
            }
            this.setState({isExpanded: true})
        }
    }

    render() {
        let { element, indexElement, parenthesesFound, extendedRowsNumber } = this.props

        return (
            <div id={'code-line-'+indexElement} className="code-line" key={indexElement}> 
                <span className="left-column">
                    <span className="numeric-column">
                        {indexElement + 1} 
                    </span>
                    {
                        (parenthesesFound[indexElement] === ADD_CONTRACT_ICON) && 
                        <img 
                            id={"img-"+indexElement}
                            onClick={() => this.handleOnClickExpandCode(indexElement, extendedRowsNumber[indexElement])} 
                            className="expand-code-icon" 
                            src={MinusSquare} 
                            alt="minus-square-image" 
                            />
                    }
                </span>
                {/* <Markup content={this.checkKeywords(element)}></Markup> */}
                { checkKeywords(element) }
                <span id={"remove-span-"+indexElement} className="remove-span">{"... }"}</span>
            </div>
        )
    }
}

export { CodeLine }