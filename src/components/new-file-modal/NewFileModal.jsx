import React from "react";

import jsonData from '../data/simulation-data.json';

import "./NewFileModal.css";


class NewFileModal extends React.Component {

    onNewFileTrigger = () => {
        this.props.parentCallback(false);
    }
    onNewFileDataTrigger = (data) => {
        this.props.parentForNewFileDataCallback(data);
    }

    addNewFile(data, fileName, commitMessage, index = 0) {
        let fullPathArray = this.props.currentPath.split('/').filter(e => e)
        let searchedFile
        data.forEach(el => {
            if (el.name === fullPathArray[index]) {
                searchedFile = el
            }
        })
        if (searchedFile) {
            if (index < fullPathArray.length - 1) {
                if (searchedFile.type === "folder") {
                    return this.addNewFile(searchedFile.children, fileName, commitMessage, index + 1)
                }
            } else {
                this.onNewFileTrigger()
                let newFileData = {
                    id: 19 + Math.floor(Math.random() * 1000),
                    type: this.props.fileType,
                    name: fileName,
                    children: [],
                    commit: commitMessage,
                    time: Date.now().toString()
                }
                searchedFile.children.push(newFileData)
                this.onNewFileDataTrigger(jsonData)
            }
        } 
    }

    render() {
        let fileName = ""
        let commitMessage = ""
            return <div className="add-new-file-modal">
                <form className="modal-form">
                    <div className="form-element-field">
                        {(this.props.fileType === "folder") ? <label>Folder name:</label> : <label>File name:</label>}
                        <input onChange={(e) => {fileName=e.target.value}} type="text" />
                    </div>
                    <div className="form-element-field">
                        <label>Commit message:</label>
                        <input onChange={(e) => {commitMessage=e.target.value}} type="text" />
                    </div>
                    <div className="modal-form-buttons-container">
                        <div 
                            onClick={() => this.addNewFile(jsonData, fileName, commitMessage)} 
                            className="add-btn"
                        >ADD</div>

                        <div 
                            onClick = {this.onNewFileTrigger}
                            className="cancel-btn"
                        >CANCEL</div>
                    </div>
                </form>
            
        </div>
        
    }
}

export { NewFileModal }