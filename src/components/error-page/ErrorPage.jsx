import React from "react";
import "./ErrorPage.css";


class ErrorPage extends React.Component {

    render() {
        return <div className="error-message">404</div>;
    }
}

export { ErrorPage }