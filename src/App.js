import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import { Page } from "./components/page/Page"
import { ErrorPage } from "./components/error-page/ErrorPage"

function App() {
    return (
        <div>
            <Switch>
                <Redirect exact from="/" to="/ProjectName" />
                <Route exact path="/Error" component={ErrorPage} />
                <Route path={["/", "/ProjectName"]} component={Page} />

            </Switch>
        </div>
    );
}

export default App;