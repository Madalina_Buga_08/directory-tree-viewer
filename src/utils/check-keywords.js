import React from "react"

export const checkKeywords = (codeLine) => {
    let codeLineArray = codeLine.split(' ')
    let codeLineNewArray = []
    for (let i = 0; i < codeLineArray.length; i++) {
        let element = codeLineArray[i]
        if (element.substring(element.length-8, element.length) === "function") {
            codeLineNewArray.push(<span className="red-keyword">{element} </span>)

        } else if (element.substring(element.length-6, element.length) === "import") {
            codeLineNewArray.push(<span className="red-keyword">{element} </span>)

        } else if (element.substring(element.length-6, element.length) === "return") {
            codeLineNewArray.push(<span className="green-keyword">{element} </span>)

        } else if (element.substring(element.length-5, element.length) === "class") {
            codeLineNewArray.push(<span className="blue-keyword">{element} </span>)

        } else if (element.substring(element.length-6, element.length) === "export") {
            codeLineNewArray.push(<span className="blue-keyword">{element} </span>)

        } else if (element.substring(element.length-4, element.length) === "div>" ||
                element.substring(element.length-3, element.length) === "div") {
            codeLineNewArray.push(<span className="red-keyword">{element} </span>)

        } else if (element.substring(element.length-7, element.length) === "extends") {
                codeLineNewArray.push(<span className="blue-keyword">{element} </span>)

        } else if (element.substring(element.length-10, element.length) === "this.state") {
            codeLineNewArray.push(<span className="yellow-keyword">{element} </span>)

        } else if (element.substring(element.length-3, element.length) === "let" || 
                element.substring(element.length-3, element.length) === "var" ||  
                element.substring(element.length-5, element.length) === "const") {
            codeLineNewArray.push(<span className="blue-keyword">{element} </span>)

        } else if (element.search("//") !== -1) {
            for (let j = i; j <= codeLineArray.length-1; j++) {
                codeLineNewArray.push(<span className="comment-line">{codeLineArray[j]} </span>)
            }
            break

        } else {
            codeLineNewArray.push(element + ' ')
        }
    }
    return codeLineNewArray
}