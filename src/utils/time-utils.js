export const displayTimeAgo = (time) => {
    let timeDifference = Date.now() - time;
    let timeTransformObject = {}
    timeTransformObject.second = 1000
    timeTransformObject.minute = timeTransformObject.second * 60
    timeTransformObject.hour = timeTransformObject.minute * 60
    timeTransformObject.day = timeTransformObject.hour * 24
    timeTransformObject.month = timeTransformObject.day * 30
    timeTransformObject.year = timeTransformObject.month * 12

    if (timeDifference < timeTransformObject.minute) {
        return Math.floor(timeDifference / timeTransformObject.second) + ' seconds ago'
    } else if (timeDifference < timeTransformObject.hour) {
        return Math.floor(timeDifference / timeTransformObject.minute) + ' minutes ago'
    } else if (timeDifference < timeTransformObject.day) {
        return Math.floor(timeDifference / timeTransformObject.hour) + ' hours ago'
    } else if (timeDifference < timeTransformObject.month) {
        return Math.floor(timeDifference / timeTransformObject.day) + ' days ago'
    } else if (timeDifference < timeTransformObject.year) {
        return Math.floor(timeDifference / timeTransformObject.month) + ' months ago'
    } else { return Math.floor(timeDifference / timeTransformObject.year) + ' years ago' }
}